package cn.bvin.app.demo;

import cn.bvin.widget.number_edittext.NumberEditText;

/**
 * Created by bvin on 2015/12/25.
 */
public class CurrencyZoom extends NumberEditText.Zoomer{
    @Override
    public int scale() {
        return 2;
    }
}
